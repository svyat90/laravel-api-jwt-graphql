<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

//Route::middleware('auth:api')->get('/user', function (Request $request) {
//    return $request->user();
//});
//
//Route::middleware('api')->get('/me', function () {
//    return json_encode(['message' => 'hello world']);
//});

//
//Route::group(['middleware' => ['api','cors']], function () {
//    Route::post('auth/register', 'Auth\ApiRegisterController@create');
//});

//Route::group(['middleware' => ['api','cors']], function () {
//    Route::post('auth/register', 'Auth\ApiRegisterController@create');
//    Route::post('auth/login', 'Auth\ApiAuthController@login');
//    Route::post('auth/logout', 'Auth\ApiAuthController@logout');
//    Route::post('auth/user', 'Auth\ApiAuthController@getAuthenticatedUser');
//    Route::post('profile/me', 'ProfileController@update');
//});
<?php
/**
 * Created by PhpStorm.
 * User: svyat
 * Date: 17.01.18
 * Time: 14:42
 */

namespace App\Http\Middleware;

use Closure;
use JWTAuth;
use Exception;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;
use Tymon\JWTAuth\Exceptions\TokenInvalidException;

class AuthJWT
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        try {

            JWTAuth::toUser($request->input('token'));

        } catch (Exception $e) {

            try {

                if (! $user = JWTAuth::parseToken()->authenticate()) {
                    return response()->json(['user_not_found'], 404);
                }

            } catch (TokenExpiredException $e) {

                return response()->json([
                    'response' => 'error',
                    'error' => 'token_expired',
                    'message' => $e->getMessage()
                ]);

            } catch (TokenInvalidException $e) {

                return response()->json([
                    'response' => 'error',
                    'error' => 'token_invalid',
                    'message' => $e->getMessage()
                ]);

            } catch (JWTException $e) {

                return response()->json([
                    'response' => 'error',
                    'error' => 'token_absent',
                    'message' => $e->getMessage()
                ]);
            }
            
        }

        return $next($request);
    }
}
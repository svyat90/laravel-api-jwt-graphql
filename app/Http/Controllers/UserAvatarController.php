<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;

class UserAvatarController extends Controller
{
    public function create(Request $request)
    {
        if(Input::hasFile('image') && Input::file('image')->isValid()) {
            $path = Input::file('image')->store('public/avatars');
        }

        return $path ? preg_replace('/^public\//', '', $path) : null;
    }

    public function update(Request $request)
    {
        $path = $request->file('avatar')->store('avatars');

        return $path;
    }
}

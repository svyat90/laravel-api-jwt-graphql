<?php

namespace App\Http\Controllers;

use JWTAuth;
use Symfony\Component\HttpFoundation\Request;

class ProfileController extends Controller
{
    public function index()
    {
        return view('profile.index');
    }

    public function edit()
    {
        return view('profile.edit');
    }
}
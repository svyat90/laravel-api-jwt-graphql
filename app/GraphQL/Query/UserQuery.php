<?php

namespace App\GraphQL\Query;

use App\User;
use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use JWTAuth;

class UserQuery extends Query
{
    private $_auth = null;

    protected $attributes = [
        'name' => 'UserQuery',
        'description' => 'A User query'
    ];

    public function authorize($root, $args)
    {
        try {
            $this->_auth = JWTAuth::parseToken()->authenticate();

            return true;

        } catch (\Exception $e) {

            $this->_auth = null;
            $this->_error = "invalid_token";

            return false;
        }
    }

    public function type()
    {
        return GraphQL::type('User');
    }

    public function args()
    {
        return [];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $user = User::find($this->_auth->id);
        return $user;
    }
}

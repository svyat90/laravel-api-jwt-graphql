<?php

namespace App\GraphQL\Query;

use Folklore\GraphQL\Support\Query;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use JWTAuth;

class AuthQuery extends Query
{

    protected $attributes = [
        'name' => 'AuthQuery',
        'description' => 'A Auth query'
    ];

    public function type()
    {
        return GraphQL::type('Auth');
    }

    public function args()
    {
        return [
            'email' => [
                'type' => Type::nonNull(Type::string())
            ],
            'password' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $error = null;

        if (! $token = JWTAuth::attempt($args)) {
            $error = "Invalid credentials";
            $token = null;
        }

        return [
            'token' => $token,
            'error' => $error
        ];
    }
}

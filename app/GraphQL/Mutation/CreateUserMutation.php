<?php

namespace App\GraphQL\Mutation;

use App\User;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;

class CreateUserMutation extends Mutation
{
    protected $attributes = [
        'name' => 'CreateUserMutation',
        'description' => 'A mutation to create User'
    ];

    public function type()
    {
        return GraphQL::type('User');
    }

    public function args()
    {
        return [
            'first_name' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required', 'min:3']
            ],
            'last_name' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required']
            ],
            'email' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['email', 'unique:users']
            ],
            'password' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['min:6', 'confirmed']
            ],
            'password_confirmation' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['min:6']
            ],
            'phone' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required']
            ],
            'picture' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required']
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $user = new User([
            'email' => $args['email'],
            'password' => bcrypt($args['password'])
        ]);

        $user->save();
        $user->profile()->create($args);
        
        return $user;
    }
}

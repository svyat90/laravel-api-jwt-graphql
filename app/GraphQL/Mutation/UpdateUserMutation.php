<?php

namespace App\GraphQL\Mutation;

use App\User;
use Folklore\GraphQL\Support\Mutation;
use GraphQL\Type\Definition\ResolveInfo;
use GraphQL\Type\Definition\Type;
use GraphQL;
use JWTAuth;

class UpdateUserMutation extends Mutation
{
    private $_auth = null;
    private $_error = null;

    protected $attributes = [
        'name' => 'UpdateUserMutation',
        'description' => 'A Update User mutation'
    ];

    public function authorize($root, $args)
    {
        try {
            $this->_auth = JWTAuth::parseToken()->authenticate();

            return true;

        } catch (\Exception $e) {

            $this->_auth = null;
            $this->_error = "invalid_token";

            return false;
        }
    }


    public function type()
    {
        return GraphQL::type('User');
    }

    public function args()
    {
        return [
            'first_name' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required']
            ],
            'last_name' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required']
            ],
            'phone' => [
                'type' => Type::nonNull(Type::string()),
                'rules' => ['required','min:10']
            ]
        ];
    }

    public function resolve($root, $args, $context, ResolveInfo $info)
    {
        $user = null;

        if ($this->_auth) {
            $user = User::find($this->_auth->id);
            $user->profile()->update($args);
        }

        return ($user) ? $user : [
            'error' => $this->_error
        ];
    }
}

<?php

namespace App\GraphQL\Type;

use GraphQL\Type\Definition\Type;
use Folklore\GraphQL\Support\Type as BaseType;
use GraphQL;

class ProfileType extends BaseType
{
    protected $attributes = [
        'name' => 'Profile',
        'description' => 'A Profile type'
    ];

    public function fields()
    {
        return [
            'id' => [
                'type' => Type::nonNull(Type::int())
            ],
            'user_id' => [
                'type' => Type::nonNull(Type::int())
            ],
            'first_name' => [
                'type' => Type::nonNull(Type::string())
            ],
            'last_name' => [
                'type' => Type::nonNull(Type::string())
            ],
            'phone' => [
                'type' => Type::nonNull(Type::string())
            ],
            'picture' => [
                'type' => Type::nonNull(Type::string())
            ]
        ];
    }
}

@extends('layouts.app')

@section('nav-bar')
    <!-- Right Side Of Navbar -->
    <ul id="nav-bar" class="nav navbar-nav navbar-right"></ul>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Profile</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <ul id="list_fields" class="list-group"></ul>

                        <p>
                            {!! Form::open(['method' => 'get', 'route' => 'edit_profile']) !!}
                            {!! Form::submit('Edit profile', ['class' => 'btn btn-primary btn-lg btn-block']) !!}
                            {!! Form::close() !!}
                        </p>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')

    <script>

        $(document).ready(function () {

            var token = localStorage.getItem('token');

            $.ajaxSetup({
                headers: {
                    "Authorization" : "Bearer " + token
                }
            });

            function setNavBar(profileInfo) {
                var data;
                if (profileInfo) {
                    data = '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">'+profileInfo.email+'<span class="caret"></span></a><ul class="dropdown-menu"><li><a href="{{ route('profile') }}">My profile</a></li><li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById("logout-form").submit();">Logout </a><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form></li></ul></li>';
                } else {
                    data = '<li><a href="{{ route('login') }}">Login</a></li><li><a href="{{ route('register') }}">Register</a></li>';
                }
                $("#nav-bar").append(data);
            }

            function setFields() {
                $.post({
                    url: 'graphql',
                    data: JSON.stringify({
                        "query": 'query { me' +
                        '{ email profile { first_name last_name phone picture } } }' }),
                    contentType: 'application/json'
                }).done(function(response) {

                    if (! response.data.me) {
                        $(location).attr('href', 'login');
                    }

                    setNavBar(response.data.me);

                    var profile = response.data.me.profile;

                    for (var item in profile) {
                        if (item == "picture") {
                            var data = '<li class="list-group-item"><b>'+item+':</b> <img src="'+'http://'+window.location.hostname+':'+window.location.port+'/storage/'+profile[item]+'" class="img-circle" width="100px"></li>';
                            $("#list_fields").append(data);
                        } else {
                            $("#list_fields").append('<li class="list-group-item"><b>'+item+':</b> '+profile[item]+'</li>')
                        }
                    }
                });
            }

            $('form').submit(function (event) {
                $(location).attr('href', 'profile/edit');
                event.preventDefault();
            });

            setFields();
        })

    </script>

@endsection
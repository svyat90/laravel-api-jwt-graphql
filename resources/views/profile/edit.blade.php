@extends('layouts.app')

@section('nav-bar')
    <!-- Right Side Of Navbar -->
    <ul id="nav-bar" class="nav navbar-nav navbar-right"></ul>
@endsection

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-8 col-md-offset-2">
                <div class="panel panel-default">
                    <div class="panel-heading">Profile</div>

                    <div class="panel-body">
                        @if (session('status'))
                            <div class="alert alert-success">
                                {{ session('status') }}
                            </div>
                        @endif

                        <ul id="list_fields" class="list-group"></ul>

                        <p>
                            {!! Form::open(['method' => 'get']) !!}
                            {!! Form::submit('Save', ['class' => 'btn btn-primary btn-lg btn-block']) !!}
                            {!! Form::close() !!}
                        </p>

                        <div id="errors" class="alert alert-danger" role="alert" style="display: none;">
                            <ul></ul>
                        </div>

                        <div id="success" class="alert alert-success" role="alert" style="display: none;">
                            <ul>
                                <li>Update has been successfully saved!</li>
                            </ul>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('footer')

    <script>

        $(document).ready(function () {

            var token = localStorage.getItem('token');

            $.ajaxSetup({
                headers: {
                    "Authorization" : "Bearer " + token
                }
            });

            function setNavBar(profileInfo) {
                var data;
                if (profileInfo) {
                    data = '<li class="dropdown"><a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">'+profileInfo.email+'<span class="caret"></span></a><ul class="dropdown-menu"><li><a href="{{ route('profile') }}">My profile</a></li><li><a href="{{ route('logout') }}" onclick="event.preventDefault();document.getElementById("logout-form").submit();">Logout </a><form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form></li></ul></li>';
                } else {
                    data = '<li><a href="{{ route('login') }}">Login</a></li><li><a href="{{ route('register') }}">Register</a></li>';
                }
                $("#nav-bar").append(data);
            }

            function createFormFields() {
                $.post({
                    url: 'http://'+window.location.hostname+':'+window.location.port+'/graphql',
                    data: JSON.stringify({
                        "query": 'query { me' +
                        '{ email profile { first_name last_name phone picture } } }' }),
                    contentType: 'application/json'
                }).done(function(response) {

                    if (! response.data.me) {
                        $(location).attr('href', window.location.hostname +'/login');
                    }

                    setNavBar(response.data.me);

                    var profile = response.data.me.profile;

                    for (var item in profile) {
                        if (item == "picture") {
                            continue;
                        } else {
                            $("#list_fields").append('<div class="form-group"><label for="' + item + '">' + item + ':</label><input class="form-control" name="' + item + '" type="text" value="' + profile[item] + '" id="' + item + '"></div>')
                        }
                    }
                });
            }

            function showErrors(data) {
                $("#errors").css('display', 'block');
                $("#success").css('display', 'none');

                data.forEach(function (errors) {
                    var list_errors = errors.validation;

                    $('#errors ul').empty();

                    for (var error in list_errors) {
                        var value = list_errors[error][0];
                        $('#errors ul').append('<li>'+value+'</li>');
                    }
                });
            }

            function showSuccess() {
                $('#errors').css('display', 'none');
                $("#success").css('display', 'block');
            }

            function saveChange() {

                var first_name = $("#first_name").val();
                var last_name = $("#last_name").val();
                var phone = $("#phone").val();
                var picture = $("#picture").val();

                $.post({
                    url: 'http://'+window.location.hostname+':'+window.location.port+'/graphql',
                    data: JSON.stringify({
                        "query": 'mutation { updateUser(' +
                        'first_name:"'+first_name+'" ' +
                        'last_name:"'+last_name+'"' +
                        'phone:"'+phone+'"' +
                        ') { email profile { first_name last_name phone picture } } }' }),
                    contentType: 'application/json'
                }).done(function(response) {

                    if (response.errors) {
                        showErrors(response.errors);
                    }

                    if (response.data.updateUser) {
                        showSuccess();
                    }

                    setTimeout(function () {
                        $(location).attr('href', 'http://'+window.location.hostname+':'+window.location.port+'/profile');
                    }, 2000);
                });
            }

            $('form').submit(function (event) {
                saveChange();
                event.preventDefault();
            });

            createFormFields();
        })

    </script>

@endsection
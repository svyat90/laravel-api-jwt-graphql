@extends('layouts.app')

@section('nav-bar')
    <!-- Right Side Of Navbar -->
    <ul id="nav-bar" class="nav navbar-nav navbar-right"></ul>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Login</div>

                <div class="panel-body">
                    <form class="form-horizontal" method="POST" action="{{ route('login') }}">

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required autofocus>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-8 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Login
                                </button>

                                <a class="btn btn-link" href="{{ route('password.request') }}">
                                    Forgot Your Password?
                                </a>
                            </div>
                        </div>
                    </form>

                    <div id="errors" class="alert alert-danger" role="alert" style="display: none;">
                        <ul></ul>
                    </div>

                    <div id="success" class="alert alert-success" role="alert" style="display: none;">
                        <ul>
                            <li>Authentication has been successfully completed!</li>
                        </ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer')

    <script>

        $(document).ready(function () {

            function showErrors(data) {
                $("#success").css('display', 'none');
                $("#errors").css('display', 'block');

                $('#errors ul').empty();
                $('#errors ul').append('<li>'+data+'</li>');
            }

            function showSuccess(token) {
                $("#success").css('display', 'block');
                $("#errors").css('display', 'none');

                setTimeout(function () {
                    localStorage.setItem('token', token);
                    $(location).attr('href', 'profile');
                }, 2000);
            }

            function login() {
                var email = $("#email").val();
                var password = $("#password").val();

                $.post({
                    url: 'graphql',
                    data: JSON.stringify({
                        "query": 'query { auth(' +
                        'email:"'+email+'"' +
                        'password:"'+password+'") { token error } }' }),
                    contentType: 'application/json'
                }).done(function(response) {

                    var token = response.data.auth.token;
                    var error = response.data.auth.error;

                    if(token) {
                        showSuccess(token);
                    }

                    if(error) {
                        showErrors(error);
                    }
                });
            }

            function setNavBar() {
                var data = '<li><a href="{{ route('login') }}">Login</a></li><li><a href="{{ route('register') }}">Register</a></li>';
                $("#nav-bar").append(data);
            }

            $('form').submit(function (event) {
                login();
                event.preventDefault();
            })

            setNavBar();
        })

    </script>

@endsection
@extends('layouts.app')

@section('nav-bar')
    <!-- Right Side Of Navbar -->
    <ul id="nav-bar" class="nav navbar-nav navbar-right"></ul>
@endsection

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <div class="panel panel-default">
                <div class="panel-heading">Register</div>

                <div class="panel-body">

                    {!! Form::open(['name' => 'register', 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal']) !!}

                        <div class="form-group{{ $errors->has('first_name') ? ' has-error' : '' }}">
                            <label for="first_name" class="col-md-4 control-label">First name</label>

                            <div class="col-md-6">
                                <input id="first_name" type="text" class="form-control" name="first_name" value="{{ old('first_name') }}" required autofocus>

                                @if ($errors->has('first_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('last_name') ? ' has-error' : '' }}">
                            <label for="last_name" class="col-md-4 control-label">Last name</label>

                            <div class="col-md-6">
                                <input id="last_name" type="text" class="form-control" name="last_name" value="{{ old('last_name') }}" required autofocus>

                                @if ($errors->has('last_name'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('phone') ? ' has-error' : '' }}">
                            <label for="phone" class="col-md-4 control-label">Phone</label>

                            <div class="col-md-6">
                                <input id="phone" type="text" class="form-control" name="phone" value="{{ old('phone') }}" required autofocus>

                                @if ($errors->has('phone'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('phone') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                            <label for="email" class="col-md-4 control-label">E-Mail Address</label>

                            <div class="col-md-6">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" required>

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                            <label for="password" class="col-md-4 control-label">Password</label>

                            <div class="col-md-6">
                                <input id="password" type="password" class="form-control" name="password" required>

                                @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password-confirm" class="col-md-4 control-label">Confirm Password</label>

                            <div class="col-md-6">
                                <input id="password_confirmation" type="password" class="form-control" name="password_confirmation" required>
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-md-6 col-md-offset-4">
                                <button type="submit" class="btn btn-primary">
                                    Register
                                </button>
                            </div>
                        </div>

                    {!! Form::close() !!}

                    {!! Form::open(['name' => 'upload_picture', 'method' => 'POST', 'files' => true, 'class' => 'form-horizontal']) !!}

                        <input type="hidden" id="_token" value="{{ csrf_token() }}">

                        <div class="form-group{{ $errors->has('picture') ? ' has-error' : '' }}">
                            <label for="picture" class="col-md-4 control-label">Picture</label>

                            <div class="col-md-6">
                                {!! Form::file('picture', ['placeholder' => 'Upload picture']) !!}
                                @if ($errors->has('picture'))
                                    <span class="help-block">
                                            <strong>{{ $errors->first('picture') }}</strong>
                                        </span>
                                @endif
                            </div>

                            <div class="col-md-6 col-md-offset-4">
                                <button id="download-picture" class="btn btn-primary">
                                    Download Picture
                                </button>
                            </div>

                        </div>

                    {!! Form::close() !!}

                    <div id="errors" class="alert alert-danger" role="alert" style="display: none;">
                        <ul></ul>
                    </div>

                    <div id="success" class="alert alert-success" role="alert" style="display: none;">
                        <ul></ul>
                    </div>

                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('footer')

    <script>

        $(document).ready(function () {
            var errors = $("#errors");
            var errors_ul = $('#errors ul');
            var success = $("#success");
            var success_ul = $('#success ul');

            var picture = null;

            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('input#_token').val()
                }
            });

            function downloadPicture() {
                var image = $('input[type=file]')[0].files[0];

                if(! image) {
                    showError('Picture not changed!');
                    return null;
                }

                var form = new FormData();
                form.append('image', image);

                $.ajax({
                    url: 'picture/create',
                    data: form,
                    cache: false,
                    contentType: false,
                    processData: false,
                    type: 'POST',
                    success:function(response) {
                        picture = response;
                        showSuccess('Picture has been successfully downloaded!')
                    }
                });
            }

            function showError(value){
                errors.css('display', 'block');
                errors_ul.empty();
                errors_ul.append('<li>'+value+'</li>');
            }

            function showErrors(data) {
                success.css('display', 'none');
                errors.css('display', 'block');
                errors_ul.empty();

                data.forEach(function (errors) {
                    var list_errors = errors.validation;

                    for (var error in list_errors) {
                        var value = list_errors[error][0];
                        errors_ul.append('<li>'+value+'</li>');
                    }
                });
            }

            function showSuccess(value, redirect) {
                errors.css('display', 'none');
                success_ul.empty();
                success.css('display', 'block');

                success_ul.append('<li>'+value+'</li>');

                if (redirect) {
                    setTimeout(function () {
                        $(location).attr('href', 'login');
                    }, 2000);
                }
            }

            function register() {

                if(! picture) {
                    showError('Picture not download!');
                    return null;
                }

                var first_name = $("#first_name").val();
                var last_name = $("#last_name").val();
                var phone = $("#phone").val();
                var email = $("#email").val();
                var password = $("#password").val();
                var password_confirmation = $("#password_confirmation").val();

                $.post({
                    url: 'graphql',
                    data: JSON.stringify({
                        "query": 'mutation { createUser(' +
                        'first_name:"'+first_name+'" ' +
                        'last_name:"'+last_name+'"' +
                        'phone:"'+phone+'"' +
                        'picture:"'+picture+'"' +
                        'email:"'+email+'"' +
                        'password:"'+password+'"' +
                        'password_confirmation:"'+password_confirmation+'") { email profile { first_name last_name } } }' }),
                    contentType: 'application/json'
                }).done(function(response) {
                    var errors = response.errors;

                    if(response.data.createUser) {
                        showSuccess('Registration has been successfully completed!', true);
                    }

                    if(errors) {
                        showErrors(errors);
                    }
                });
            }

            function setNavBar() {
                var data = '<li><a href="{{ route('login') }}">Login</a></li><li><a href="{{ route('register') }}">Register</a></li>';
                $("#nav-bar").append(data);
            }

            $("form[name=upload_picture]").submit(function (event) {
                event.preventDefault();
                downloadPicture();
            });

            $('form[name=register]').submit(function (event) {
                event.preventDefault();
                register();
            });

            setNavBar();
        })

    </script>

@endsection